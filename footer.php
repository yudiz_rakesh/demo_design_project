<style type="text/css">
    /* -=- Footer CSS Start -=- */
    footer                              { background: #141618; }
    .insta-feeds                        { display: flex; display: -webkit-flex; }
    .social-icons                       { display: flex; display: -webkit-flex; justify-content: center; }
    .social-icons > li                  { margin-right: 18px; }
    .social-icons > li:last-child       { margin-right: 0px; }
    .social-icons > li > a              { padding: 4px; display: block; }
    .social-icons > li > a > i:before   { font-size: 18px; color: #fff; transition-duration: 0.5s; -webkit-transition-duration: 0.5s; }
    .social-icons > li > a:hover > i:before   { color: #b4975a; }
</style>
<footer class="cmn-spacing">
    <div class="container">
        <h2 class="text-center white-txt">Social Links</h2>
        <!-- <ul class="insta-feeds">
            <li>
                <div class="feeds-wrap">
                </div>
            </li>
        </ul> -->
        <ul class="social-icons">
            <li><a target="_blank" href="www.facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="www.twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="www.pinterest.com"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="www.dribble.com"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="www.linkedin.com"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="www.vimeo.com"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
            <li><a target="_blank" href="www.tumblr.com"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</footer>